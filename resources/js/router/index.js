import {createRouter, createWebHistory} from "vue-router";

import Home from "../views/Home.vue";
import RickMorty from "../views/rickMorty";
import Contact from "../views/Contact";

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/rickAndMorty',
        name: 'rickMorty',
        component: RickMorty
    },
    {
        path: '/contact',
        name: 'contact',
        component: Contact
    }
]


export default createRouter({
    history: createWebHistory(),
    routes,
});
