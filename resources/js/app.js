import './bootstrap';

import { createApp } from "vue";
import App from "./App.vue";

// components
import NavbarComponent from "./components/NavbarComponent.vue";

import router from "./router";

createApp(App)
    .component('navbar', NavbarComponent)
    .use(router)
    .mount('#app')
